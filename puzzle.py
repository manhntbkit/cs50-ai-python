from logic import *

AKnight = Symbol("A is a Knight")
AKnave = Symbol("A is a Knave")

BKnight = Symbol("B is a Knight")
BKnave = Symbol("B is a Knave")

CKnight = Symbol("C is a Knight")
CKnave = Symbol("C is a Knave")

# Puzzle 0
# A says "I am both a knight and a knave."
knowledge0 = And(
    Implication(And(AKnave, AKnight), AKnight),
    Implication(Not(And(AKnave, AKnight)), AKnave),
    Not(And(AKnave, AKnight)),
    Or(AKnave, AKnight)
)

# Puzzle 1
# A says "We are both knaves."
# B says nothing.
knowledge1 = And(
    Implication(And(AKnave, BKnave), AKnight),
    Implication(Not(And(AKnave, BKnave)), AKnave),
    Not(And(AKnave, AKnight)),
    Or(AKnave, AKnight),
    Not(And(BKnave, BKnight)),
    Or(BKnave, BKnight)
)

# Puzzle 2
# A says "We are the same kind."
# B says "We are of different kinds."
knowledge2 = And(
    Implication(Or(And(AKnave, BKnave), And(AKnight, BKnight)), AKnight),
    Implication(Not(Or(And(AKnave, BKnave), And(AKnight, BKnight))), AKnave),
    Implication(Or(And(AKnave, BKnight), And(AKnight, BKnave)), BKnight),
    Implication(Not(Or(And(AKnave, BKnight), And(AKnight, BKnave))), BKnave),
    Not(And(AKnave, AKnight)),
    Or(AKnave, AKnight),
    Not(And(BKnave, BKnight)),
    Or(BKnave, BKnight)
)

# Puzzle 3
# A says either "I am a knight." or "I am a knave.", but you don't know which.
# B says "A said 'I am a knave'."
# B says "C is a knave."
# C says "A is a knight."
Asaid = Symbol("A said 'I am a knave'")
knowledge3 = And(
    # A says either "I am a knight." or "I am a knave.", but you don't know which.
    #Implication(AKnight, Or(AKnight, AKnave))

    Implication(Or(AKnight, AKnave), AKnight),
    Implication(Not(Or(AKnight, AKnave)), AKnave),

    # B says "A said 'I am a knave'."
    # I only care whether B says True or False.
    Implication(Asaid,BKnight),
    Implication(Not(Asaid),BKnave),

    # B says "C is a knave."
    Implication(CKnave, BKnight),
    Implication(Not(CKnave), BKnave),

    # C says "A is a knight."
    Implication(AKnight, CKnight),
    Implication(Not(AKnight), CKnave),

    Or(AKnave, AKnight),
    Not(And(AKnave, AKnight)),
    Or(BKnave, BKnight),
    Not(And(BKnave, BKnight)),
    Or(CKnave, CKnight),
    Not(And(CKnave, CKnight))
    
)


def main():
    symbols = [AKnight, AKnave, BKnight, BKnave, CKnight, CKnave]
    puzzles = [
        ("Puzzle 0", knowledge0),
        ("Puzzle 1", knowledge1),
        ("Puzzle 2", knowledge2),
        ("Puzzle 3", knowledge3)
    ]
    for puzzle, knowledge in puzzles:
        print(puzzle)
        if len(knowledge.conjuncts) == 0:
            print("    Not yet implemented.")
        else:
            for symbol in symbols:
                if model_check(knowledge, symbol):
                    print(f"    {symbol}")


if __name__ == "__main__":
    main()
