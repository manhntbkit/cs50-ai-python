"""
Tic Tac Toe Player
"""

import math, pdb, copy
# variable to represent possible moves of the board.
X = "X"
O = "O"
EMPTY = None


def initial_state():
    """
    Returns starting state of the board.
    """
    #pdb.set_trace()
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
    Returns player who has the next turn on a board.
    compare number of X and number of Y.
        count number of X and Y in board(current state)
    """
    count_x = 0
    count_o = 0
    for row in board:
        count_x += row.count(X)
        count_o += row.count(O)
    if count_o >= count_x:
        return X
    else:
        return O


def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    available_cells = set()
    for row_index in range(3):
        for colum_index in range(3):
            if board[row_index][colum_index] == EMPTY:
                available_cells.add((row_index, colum_index))
    return available_cells

def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    
    #pdb.set_trace()
    if action == None or action[0] >2 or action[1] >2:
        raise ValueError('invalid action')
    new_board = copy.deepcopy(board)
    new_board[action[0]][action[1]] = player(board)
    return new_board

def winner(board):
    """
    Returns the winner of the game, if there is one.
    return None if no one.
    """
    #row level.
    for row in board:
        #check at row level.
        is_win_at_row = (row[0] == row[1] == row[2])
        if is_win_at_row == True and row[0] != EMPTY:
            #return X or O
            return row[0]

    # column level
    col1 = (board[0][0] == board[1][0] == board[2][0])
    if col1 == True and col1 != EMPTY:
        return board[0][0]

    col2 = (board[0][1] == board[1][1] == board[2][1])
    if col2 == True and col2 != EMPTY:
        return board[0][1]

    col3 = (board[0][2] == board[1][2] == board[2][2])
    if col3 == True and col3 != EMPTY:
        return board[0][2]
    #diagonally level.
    dg1 = (board[0][0] == board[1][1] == board[2][2])
    if dg1 == True and dg1 != EMPTY:
        return board[0][0]
    
    dg2 = (board[2][0] == board[1][1] == board[0][2])
    if dg2 == True and dg2 != EMPTY:
        return board[2][0]
    return None

def terminal(board):
    """
    Returns True if game is over.
    Otherwise, the function should return False if the game is still in progress.
    """
    win_player = winner(board)
    #if there is one winner, return True
    if(win_player != None):
        return True

    #check if can move, return False(in prgoress)
    if len(actions(board)) != 0:
        return False
    else:
        return True

def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """
    if winner(board) == X:
        return 1
    if winner(board) == O:
        return -1
    return 0

def minimax(board):
    """
    The minimax function should take a board as input, 
    and return the optimal move for the player to move on that board.
    The move returned should be the optimal action (i, j) that is one of the allowable actions on the board. 
    If multiple moves are equally optimal, any of those moves is acceptable.
    If the board is a terminal board, the minimax function should return None.

    Returns: action(i, j) for the current player on the board.

    MAX picks action a in ACTIONS(s) that produces
        highest value of MIN-VALUE(RESULT(s, a))
    """
    #If the board is a terminal board, the minimax function should return None.
    if terminal(board) == True:
        return None
    # The move returned should be the optimal action (i, j) that is one of the allowable actions on the board. 
    # If multiple moves are equally optimal, any of those moves is acceptable.

    curentPlayer = player(board)
    action_result = (0,0)
    if curentPlayer == X:
        #MAX player 
        # loop through available actions, to get Max value of min_value of new board
        max_value_found = -2
        for action in actions(board):#3 actions
            new_board = result(board,action)
            value = min_value(new_board)
            if max_value_found < value: 
                max_value_found = value
                action_result = action
        #end of loop, max_value is 1
        
        #max_value_found = max_value(board)#return 9
        print('max_value_found')
        print(max_value_found)
        print('action_result')
        print(action_result)
        return action_result
        
    else:
       #MIN player 
        # loop through available actions, to get Min value of max_value of new board
        min_value_found = 2
        for action in actions(board):#3 actions
            new_board = result(board,action)
            value = max_value(new_board)
            if min_value_found > value: 
                min_value_found = value
                action_result = action
        #end of loop
        return action_result

def max_value(board):
    # If reach terminal, return 1,-1, or 0
    if terminal(board) == True:
        return utility(board)
    """
    function MAX-VALUE(state)
        for action in ACTIONS(state):
            v = MAX(v, MIN-VALUE(RESULT(state, action))) 
    """
    # else loop through all available actions.
    max_result = -2
    for action in actions(board):#5,3,9 to find max value
        score_new_board = min_value(result(board, action))
        #get max value 
        if max_result < score_new_board:
            max_result = score_new_board
    return max_result

def min_value(board):
    if terminal(board) == True:
        return utility(board)

    min_result = 2 #any number greater than 1
    for action in actions(board):
        #result: Returns the board that results from making move(i, j) on the board.
        score_new_board = max_value(result(board, action))
        #get min value 
        if min_result > score_new_board:
            min_result = score_new_board
    return min_result

def player_score(player):
    if player == X:
        return 1
    if player == O:
        return -1
    return 0